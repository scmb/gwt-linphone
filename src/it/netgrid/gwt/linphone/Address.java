package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class Address extends JavaScriptObject {
	protected Address() {
	}

	public final native String asString() /*-{
		return this.asString();
	}-*/;

	public final native String asStringUriOnly() /*-{
		return this.asStringUriOnly();
	}-*/;

	public final native Address newClone() /*-{
		return this.clone();
	}-*/;

	public final native String getDisplayName() /*-{
		return this.displayName;
	}-*/;

	public final native String getDomain() /*-{
		return this.domain;
	}-*/;

	public final native int getPort() /*-{
		return this.port
	}-*/;

	public final native String getScheme() /*-{
		return this.scheme;
	}-*/;

	public final native String getUsername() /*-{
		return this.username;
	}-*/;

	public final native void setDisplayName(String displayName) /*-{
		this.displayName = displayName;
	}-*/;

	public final native void setDomain(String domain) /*-{
		this.domain = domain;
	}-*/;

	public final native void setPort(int port) /*-{
		this.port = port;
	}-*/;

	public final native void setUsername(String username) /*-{
		this.username = username;
	}-*/;

	public final native boolean weakEqual(Address other) /*-{
		return this.weakEqual(other);
	}-*/;
}
