package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class AuthenticationInfo extends JavaScriptObject {

	protected AuthenticationInfo() {
	}

	public final native String getHa1() /*-{
		return this.ha1;
	}-*/;

	public final native String getPassword() /*-{
		return this.passwd;
	}-*/;

	public final native String getRealm() /*-{
		return this.realm;
	}-*/;

	public final native String getUserid() /*-{
		return this.userid;
	}-*/;

	public final native String getUsername() /*-{
		return this.username;
	}-*/;

	public final native void setHa1(String ha1) /*-{
		this.ha1 = ha1;
	}-*/;

	public final native void setPassword(String password) /*-{
		this.passwd = password;
	}-*/;

	public final native void setRealm(String realm) /*-{
		this.realm = realm;
	}-*/;

	public final native void setUserid(String userid) /*-{
		this.userid = userid;
	}-*/;

	public final native void setUsername(String username) /*-{
		this.username = username;
	}-*/;
}
