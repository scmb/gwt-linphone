package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class Call extends JavaScriptObject {
	protected Call() {
	}

	public final native CallStats getAudioStats() /*-{
		return this.audioStats;
	}-*/;

	public final native int getAverageQuality() /*-{
		return this.averageQuality;
	}-*/;

	public final native CallLog getCallLog() /*-{
		return this.callLog;
	}-*/;

	// TODO implementa tramite enum
	public final native int getCallState() /*-{
		return this.callState;
	}-*/;

	public final native boolean getCameraEnabled() /*-{
		return this.cameraEnabled;
	}-*/;

	public final native CallParameters getCurrentCallParameters() /*-{
		return this.currentParams;
	}-*/;

	public final native int getCurrentQuality() /*-{
		return this.currentQuality;
	}-*/;

	// TODO implementa tramite enum
	public final native int getDirection() /*-{
		return this.dir;
	}-*/;

	public final native int getDuration() /*-{
		return this.duration;
	}-*/;

	public final native boolean getEchoCancellationEnabled() /*-{
		return this.echoCancellationEnabled;
	}-*/;

	public final native boolean getEchoLimiterEnabled() /*-{
		return this.echoLimiterEnabled;
	}-*/;

	public final native int getPlayVolume() /*-{
		return this.playVolume;
	}-*/;

	// TODO implementa tramite enum
	public final native int getReason() /*-{
		return this.reason;
	}-*/;

	public final native int getRecordVolume() /*-{
		return this.recordVolume;
	}-*/;

	public final native String getReferTo() /*-{
		return this.referTo;
	}-*/;

	public final native Address getRemoteAddress() /*-{
		return this.remoteAddress;
	}-*/;

	public final native String getRemoteAddressAsString() /*-{
		return this.remoteAddressAsString;
	}-*/;

	public final native String getRemoteContact() /*-{
		return this.remoteContact;
	}-*/;

	public final native CallParameters getRemoteParameters() /*-{
		return this.remoteParams;
	}-*/;

	public final native String getRemoteUserAgent() /*-{
		return this.remoteUserAgent;
	}-*/;

	public final native Call getReplacedCall() /*-{
		return this.replacedCall;
	}-*/;

	public final native Call getTransferCall() /*-{
		return this.transferCall;
	}-*/;

	public final native CallStats getVideoStats() /*-{
		return this.videoStats;
	}-*/;

	public final native boolean hasTransferPending() /*-{
		return this.hasTransferPending();
	}-*/;

	public final native void startRecording() /*-{
		this.startRecording();
	}-*/;

	public final native void stopRecording()/*-{
		this.stopRecording();
	}-*/;

	public final native int takeVideoSnapshot(String filename) /*-{
		return this.takeVideoSnapshot(filename);
	}-*/;

}
