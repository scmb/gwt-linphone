package it.netgrid.gwt.linphone;

import java.util.Date;

import com.google.gwt.core.client.JavaScriptObject;

public class CallLog extends JavaScriptObject {

	protected CallLog() {}

	public final native int getDirection()/*-{
		return this.dir;
	}-*/;

	public final native int getDuration() /*-{
		return this.duration;
	}-*/;

	public final native Address getFrom() /*-{
		return this.from;
	}-*/;

	public final native RtpStats getLocalStats()/*-{
		return this.localStats;
	}-*/;

	public final native int getQuality() /*-{
		return this.quality;
	}-*/;

	public final native String getRefKey() /*-{
		return this.refKey;
	}-*/;

	public final native Address getRemoteAddress() /*-{
		return this.remoteAddress;
	}-*/;

	public final native RtpStats getRemoteStats() /*-{
		return this.remoteStats;
	}-*/;

	// TODO Sistemare parsing date
	public final native Date getStartDate() /*-{
		return this.startDate;
	}-*/;

	// TODO Gestire con enum
	public final native int getStatus()/*-{
		return this.status;
	}-*/;

	public final native Address getTo()/*-{
		return this.to;
	}-*/;

	public final native boolean getVideoEnabled() /*-{
		return this.videoEnabled;
	}-*/;

	public final native String toHumanReadableString() /*-{
		return this.toStr();
	}-*/;

}
