package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class CallParameters extends JavaScriptObject {
	protected CallParameters() {}

	public final native void addCustomHeader(String key, String value) /*-{
		return this.addCustomHeader(key,value);
	}-*/;

	public final native String getCustomHeader(String key) /*-{
		return this.getCustomHeader(key);
	}-*/;

	public final native int getAudioBandwidthLimit() /*-{
		return this.audioBandwidthLimit;
	}-*/;

	public final native void setAudioBandwidthLimit(int bandwidth) /*-{
		this.audioBandwidthLimit = bandwidth;
	}-*/;

	public final native boolean getEarlyMediaSendingEnabled() /*-{
		return this.earlyMediaSendingEnabled;
	}-*/;

	public final native void setEarlyMediaSendingEnabled(boolean enabled) /*-{
		this.earlyMediaSendingEnabled = enabled;
	}-*/;

	public final native int getMediaEncryption() /*-{
		return this.mediaEncryption;
	}-*/;

	public final native void setMediaEncryption(int mediaEncryption) /*-{
		this.mediaEncryption = mediaEncryption;
	}-*/;

	public final native PrivacyMask getPrivacy() /*-{
		return this.privacy;
	}-*/;

	public final native void setPrivacy(PrivacyMask privacy) /*-{
		this.privacy = privacy;
	}-*/;

	public final native VideoSize getReceivedVideoSize() /*-{
		return this.receivedVideoSize;
	}-*/;

	public final native String getRecordFile() /*-{
		return this.recordFile;
	}-*/;

	public final native void setRecordFile(String recordFile) /*-{
		this.recordFile = recordFile;
	}-*/;

	public final native VideoSize getSentVideoSize() /*-{
		return this.sentVideoSize;
	}-*/;

	// TODO implementa tramite enum
	public final native int getUsedAudioCodec() /*-{
		return this.usedAudioCodec;
	}-*/;

	// TODO implementa tramite enum
	public final native int getUsedVideoCodec() /*-{
		return this.usedVideoCodec;
	}-*/;

	public final native boolean getVideoEnabled() /*-{
		return this.videoEnabled;
	}-*/;

	public final native void setVideoEnabled(boolean videoEnabled) /*-{
		this.videoEnabled = videoEnabled;
	}-*/;

	public final native CallParameters copy() /*-{
		return this.copy();
	}-*/;

	public final native boolean localConferenceMode() /*-{
		return this.localConferenceMode();
	}-*/;

}
