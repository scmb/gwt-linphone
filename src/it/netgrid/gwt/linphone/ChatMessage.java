package it.netgrid.gwt.linphone;

import it.netgrid.gwt.linphone.handler.IChatMessageStateChangedHandler;

import java.util.Date;

import com.google.gwt.core.client.JavaScriptObject;

public class ChatMessage extends JavaScriptObject {
	protected ChatMessage() {}

	public final native ChatRoom getChatRoom() /*-{
		return this.chatRoom;
	}-*/;

	public final native String getExternalBodyUrl() /*-{
		return this.externalBodyUrl;
	}-*/;

	public final native void setExternalBodyUrl(String externalBody) /*-{
		this.externalBodyUrl = externalBody;
	}-*/;

	public final native Address getFrom() /*-{
		return this.from;
	}-*/;

	public final native void setFrom(Address address) /*-{
		this.from = address;
	}-*/;

	public final native Address getLocalAddress() /*-{
		return this.localAddress;
	}-*/;

	public final native boolean isOutgoing() /*-{
		return this.outgoing;
	}-*/;

	public final native Address getPeerAddress() /*-{
		return this.peerAddress;
	}-*/;

	public final native boolean isRead() /*-{
		return this.read;
	}-*/;

	// TODO implementa con enum
	public final native int getState() /*-{
		return this.state;
	}-*/;

	public final native int getStorageId() /*-{
		return this.storageId;
	}-*/;

	public final native String getText() /*-{
		return this.text;
	}-*/;

	// TODO verifica wrapping date
	public final native Date getTime() /*-{
		return this.time;
	}-*/;

	public final native Address getTo() /*-{
		return this.to;
	}-*/;

	public final native void addCustomHeader(String key, String value) /*-{
		this.addCustomHeader(key, value);
	}-*/;

	public final native ChatMessage newClone() /*-{
		return this.clone();
	}-*/;

	public final native String getCustomHeader(String key) /*-{
		return this.getCustomHeader(key);
	}-*/;

	public final native void setStateChangedHandler(IChatMessageStateChangedHandler handler) /*-{
		this.stateChanged = handler.@it.netgrid.gwt.linphone.handler.IChatMessageStateChangedHandler::onMessageStateChanged(Lit/netgrid/gwt/linphone/ChatMessage;ILcom/google/gwt/core/client/JavaScriptObject;);
	}-*/;

}
