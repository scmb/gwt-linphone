package it.netgrid.gwt.linphone;

import it.netgrid.gwt.linphone.handler.IChatMessageStateChangedHandler;

import java.util.Date;

import com.google.gwt.core.client.JavaScriptObject;

public class ChatRoom extends JavaScriptObject {
	protected ChatRoom() {}

	public final native Linphone getCore() /*-{
		return this.lc;
	}-*/;

	public final native Address getPeerAddress() /*-{
		return this.peerAddress;
	}-*/;

	public final native ChatMessage newMessage(String message) /*-{
		return this.newMessage(message);
	}-*/;

	// TODO Verificare wrapping date
	public final native ChatMessage newMessage(String message, String externalBodyUrl, int chatMessageState, Date time, boolean isRead, boolean isIncoming) /*-{
		return this.newMessage(message, externalBodyUrl, chatMessageState, time, isRead, isIncoming);
	}-*/;

	public final native void sendMessage(ChatMessage message, IChatMessageStateChangedHandler handler, JavaScriptObject userData) /*-{
		this.sendMessage(message, handler.@it.netgrid.gwt.linphone.handler.IChatMessageStateChangedHandler::onMessageStateChanged(Lit/netgrid/gwt/linphone/ChatMessage;ILcom/google/gwt/core/client/JavaScriptObject;), userData);
	}-*/;
}
