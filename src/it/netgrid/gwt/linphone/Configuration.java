package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class Configuration extends JavaScriptObject {
	protected Configuration() {}

	public final native void cleanSection(String section)/*-{
		this.cleanSection(section);
	}-*/;

	public final native float getFloat(String section, String key, float defaultValue)/*-{
		return this.getFloat(section, key, defaultValue);
	}-*/;

	public final native int getInt(String section, String key, int defaultValue)/*-{
		return this.getInt(section, key, defaultValue);
	}-*/;

	public final native float getInt64(String section, String key, float defaultValue)/*-{
		return this.getInt64(section, key, defaultValue);
	}-*/;

	public final native boolean getRange(String section, String key, float min, float max, float defaultMin, float defaultMax)/*-{
		return this.getRange(section, key, min, max, defaultMin, defaultMax);
	}-*/;

	public final native int hasSection(String section)/*-{
		return this.hasSection(section);
	}-*/;

	public final native int readFile(String filename) /*-{
		return this.readFile(filename);
	}-*/;

	public final native void setFloat(String section, String key, float value) /*-{
		this.setFloat(section, key, value);
	}-*/;

	public final native void setInt(String section, String key, int value)/*-{
		this.setInt(section, key, value);
	}-*/;

	public final native void setInt64(String section, String key, float value)/*-{
		this.setInt64(section, key, value);
	}-*/;

	public final native void setIntHex(String section, String key, int value) /*-{
		this.setIntHex(section, key, value);
	}-*/;

	public final native void setRange(String section, String ket, float min, float max)/*-{
		this.setRange(section, ket, min, max);
	}-*/;

	public final native void sync()/*-{
		this.sync();
	}-*/;

}
