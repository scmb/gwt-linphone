package it.netgrid.gwt.linphone;

import it.netgrid.gwt.linphone.handler.IFileActionCompletedHandler;

import com.google.gwt.core.client.JavaScriptObject;

public class FileManager extends JavaScriptObject {
	
	protected FileManager() {}

	public final native FileTransfer copy(String source, String target, IFileActionCompletedHandler handler) /*-{

		var callback = function(result,message) {
			if(result)
			{
				handler.@it.netgrid.gwt.linphone.handler.IFileActionCompletedHandler::onSuccess()();
			} else {
				handler.@it.netgrid.gwt.linphone.handler.IFileActionCompletedHandler::onError(Ljava/lang/String;)(message);
			}
		};

		this.copy(source, target, callback);
	}-*/;

	public final native void exists(String url) /*-{

		var callback = function(result,message) {
			if(result)
			{
				handler.@it.netgrid.gwt.linphone.handler.IFileActionCompletedHandler::onSuccess()();
			} else {
				handler.@it.netgrid.gwt.linphone.handler.IFileActionCompletedHandler::onError(Ljava/lang/String;)(message);
			}
		};

		this.exists(url, callback);
	}-*/;

	public final native void mkdir(String url) /*-{

		var callback = function(result,message) {
			if(result)
			{
				handler.@it.netgrid.gwt.linphone.handler.IFileActionCompletedHandler::onSuccess()();
			} else {
				handler.@it.netgrid.gwt.linphone.handler.IFileActionCompletedHandler::onError(Ljava/lang/String;)(message);
			}
		};

		this.mkdir(url, callback);
	}-*/;

	public final native void remove(String url) /*-{

		var callback = function(result,message) {
			if(result)
			{
				handler.@it.netgrid.gwt.linphone.handler.IFileActionCompletedHandler::onSuccess()();
			} else {
				handler.@it.netgrid.gwt.linphone.handler.IFileActionCompletedHandler::onError(Ljava/lang/String;)(message);
			}
		};

		this.remove(url, callback);
	}-*/;
}
