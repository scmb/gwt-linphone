package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;


public class FileTransfer extends JavaScriptObject {
	
	protected FileTransfer() {}

	public final native String getErrorMessage() /*-{
		return this.error;
	}-*/;

	public final native boolean isDone() /*-{
		return this.done;
	}-*/;

	public final native int getTotalBytes() /*-{
		return this.totalBytes;
	}-*/;

	public final native int getReceivedBytes() /*-{
		return this.receivedBytes;
	}-*/;

	public final native String getSourceUrl() /*-{
		return this.sourceUrl;
	}-*/;

	public final native String getTargetUrl() /*-{
		return this.targetUrl;
	}-*/;

	public final native void start() /*-{
		this.start();
	}-*/;

	public final native void cancel() /*-{
		this.cancel();
	}-*/;
}
