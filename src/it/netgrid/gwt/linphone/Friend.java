package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class Friend extends JavaScriptObject {
	protected Friend() {}

	public final native Address getAddress()/*-{
		return this.address;
	}-*/;

	public final native void setAddress(Address address)/*-{
		this.address = address;
	}-*/;

	// TODO implementa usando enum
	public final native int getIncomingSubscribePolicy()/*-{
		return this.incSubscribePolicy;
	}-*/;

	public final native void setIncomingSubscribePolicy(int policy) /*-{
		this.incSubscribePolicy = policy;
	}-*/;

	public final native PresenceModel getPresenceModel() /*-{
		return this.presenceModel;
	}-*/;

	public final native String getReferenceKey()/*-{
		return this.refKey;
	}-*/;

	public final native void setReferenceKey(String key)/*-{
		this.refKey = key;
	}-*/;

	public final native boolean isSubscribesEnabled()/*-{
		return this.subscribesEnabled;
	}-*/;

	public final native void setSubscribesEnabled(boolean enabled) /*-{
		this.subscribesEnabled = enabled;
	}-*/;

	public final native void done()/*-{
		this.done();
	}-*/;

	public final native void edit()/*-{
		this.edit();
	}-*/;

	public final native boolean inList() /*-{
		return this.inList();
	}-*/;
}
