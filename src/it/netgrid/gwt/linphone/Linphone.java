package it.netgrid.gwt.linphone;

import it.netgrid.gwt.linphone.enums.ErrorCode;
import it.netgrid.gwt.linphone.handler.ICallStateChangedHandler;
import it.netgrid.gwt.linphone.handler.ILoadCompletedHandler;
import it.netgrid.gwt.linphone.handler.ILogHandler;
import it.netgrid.gwt.linphone.handler.IRegistrationStateChangedHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.dom.client.ObjectElement;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;

public class Linphone {

	private static SimplePanel dock;

	private static Map<String, Linphone> instances = new HashMap<String, Linphone>();

	private static final native ObjectElement buildCoreElement(String uuid, Linphone delegate) /*-{
		var objectItem = $wnd.document.createElement("object");
		objectItem.setAttribute("id", uuid);
		objectItem.setAttribute("type", "application/x-linphone-web");
		objectItem.setAttribute("width", "0");
		objectItem.setAttribute("height", "0");
		var paramItem = $wnd.document.createElement("param");
		paramItem.setAttribute("name", "onload");
		paramItem.setAttribute("value", "window.GwtLinphone['" + uuid + "']");
		//objectItem.appendChild(paramItem);

		if ("undefined" == typeof $wnd.GwtLinphone) {
			$wnd.GwtLinphone = new Array();
		}

		$wnd.GwtLinphone[uuid] = function() {
			delegate.@it.netgrid.gwt.linphone.Linphone::onLoadCompleted();
		};

		return objectItem;
	}-*/;

	private static SimplePanel getDock() {
		if (Linphone.dock == null) {
			Linphone.dock = new SimplePanel();
			Linphone.dock.getElement().setAttribute("style",
					"position:absolute;width:0;height:0;top:-100;left:-100");
			RootPanel.getBodyElement().appendChild(Linphone.dock.getElement());
		}

		return Linphone.dock;
	}

	public static void loadCompleted(String uuid) {
		if (Linphone.instances.containsKey(uuid)) {
			Linphone.instances.get(uuid).onLoadCompleted();
		}
	}

	private final List<ILoadCompletedHandler> loadCompletedHandlers;

	private final String uuid;

	private final ObjectElement coreElement;

	public Linphone() {
		this.uuid = UUID.uuid();
		this.loadCompletedHandlers = new ArrayList<ILoadCompletedHandler>();
		Linphone.instances.put(this.uuid, this);
		this.coreElement = Linphone.buildCoreElement(this.uuid,this);
		Linphone.getDock().getElement().appendChild(this.coreElement);
	}

	public Linphone(ILoadCompletedHandler handler) {
		this.uuid = UUID.uuid();
		this.loadCompletedHandlers = new ArrayList<ILoadCompletedHandler>();
		this.loadCompletedHandlers.add(handler);
		Linphone.instances.put(this.uuid, this);
		this.coreElement = Linphone.buildCoreElement(this.uuid,this);
		Linphone.getDock().getElement().appendChild(this.coreElement);
	}

	public final native CallParameters createDefaultCallParameters() /*-{

	}-*/;

	public final native void addCallStateChangedHandler(ICallStateChangedHandler handler) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		var callback = function(a,b,c,d) {
			handler.@it.netgrid.gwt.linphone.handler.ICallStateChangedHandler::onCallStateChanged(Lcom/google/gwt/core/client/JavaScriptObject;ILjava/lang/String;)(b,c,d);
		};

		if("undefined" == typeof core.addEventListener) {
			core.attachEvent("oncallStateChanged",callback);
		} else {
			core.addEventListener("callStateChanged",callback);
		}
	}-*/;

	public final native void addRegistrationStateChangedHandler(IRegistrationStateChangedHandler handler) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		
		var callback = function(a,b,c,d) {
			var code = @it.netgrid.gwt.linphone.enums.RegistrationState::getByCode(I)(c)
			handler.@it.netgrid.gwt.linphone.handler.IRegistrationStateChangedHandler::onRegistrationStateChanged(Lcom/google/gwt/core/client/JavaScriptObject;Lit/netgrid/gwt/linphone/enums/RegistrationState;Ljava/lang/String;)(b,code,d);
		};

		if("undefined" == typeof core.addEventListener) {
			core.attachEvent("onregistrationStateChanged",callback);
		} else {
			core.addEventListener("registrationStateChanged", callback, false);
		}
	}-*/;

	public final native FileManager getFileManager() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.fileManager;
	}-*/;

	public final native void addAuthenticationInfo(AuthenticationInfo authInfo) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.addAuthInfo(authInfo);
	}-*/;

	public void addLoadCompletedHandler(ILoadCompletedHandler handler) {
		this.loadCompletedHandlers.add(handler);
	}

	public final native void addProxyConfig(ProxyConfig proxy) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.addProxyConfig(proxy);
	}-*/;

	public final ErrorCode init() {
		return ErrorCode.getByCode(this.doInit());
	}

	public final ErrorCode init(String userConfig) {
		return ErrorCode.getByCode(this.doInit(userConfig));
	}

	public final ErrorCode init(String userConfig, String factoryConfig) {
		return ErrorCode.getByCode(this.doInit(userConfig, factoryConfig));
	}

	private final native int doInit() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		if("undefined" == typeof core) {
			return -100
		}
		return core.init();
	}-*/;

	private final native int doInit(String userConfig)  /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.init(userConfig);
	}-*/;

	private final native int doInit(String userConfig, String factoryConfig)  /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.init(userConfig, factoryConfig);
	}-*/;

	public final native void inviteAddress(Address address) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.inviteAdddress(address);
	}-*/;
	
	public final native Address newAddress(String address) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newAddress(address);
	}-*/;

	public final native AuthenticationInfo newAuthenticationInfo(
			String username, String userid, String password, String ha1, String realm) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newAuthInfo(username, userid, password, ha1, realm);
	}-*/;

	public final native ProxyConfig newProxyConfig() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newProxyConfig();
	}-*/;

	private void onLoadCompleted() {
		for (ILoadCompletedHandler handler : this.loadCompletedHandlers) {
			handler.onLoadCompleted(this);
		}
	}

	public final native void setDefaultProxy(ProxyConfig proxy) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.defaultProxy = proxy;
	}-*/;
	
//	public final native String getAuthInfoType() /*-{
//		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
//		var retval = core.newAuthInfo("CL01",null,"trixbox",null,null);
//		
//		if(retval == null)
//			return "is null..";
//			
//		return "crappy library";
//	}-*/;

	public final native void setIterateEnabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.iterateEnabled = enabled;
	}-*/;

	public final native boolean getIterateEnabled() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.iterateEnabled;
	}-*/;

	public final native void setIterateInterval(int interval) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.iterateInterval = interval;
	}-*/;

	public final native int getIterateInterval() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.iterateInterval;
	}-*/;

	public final native void setLogHandler(ILogHandler handler) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.logHandler = handler.@it.netgrid.gwt.linphone.handler.ILogHandler::onMessage(Ljava/lang/String;Ljava/lang/String;)
		core.iterateEnabled = true;
	}-*/;

	public final native String getMagic() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.magic;
	}-*/;

	public final native String setMagic(String magic) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.magic = magic;
	}-*/;

	public final native String getPluginVersion() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.pluginVersion;
	}-*/;

	public final native String getVersion() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.version;
	}-*/;

	public final native boolean isAdaptiveRateControlEnabled() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.adaptiveRateControlEnabled;
	}-*/;

	public final native void setAdaptiveRateControlEnabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.adaptiveRateControlEnabled = enabled;
	}-*/;

	public final native boolean isAudioAdaptiveJittcompEnabled() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.audioAdaptiveJittcompEnabled;
	}-*/;

	public final native void setAudioAdaptiveJittcompEnabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.audioAdaptiveJittcompEnabled = enabled;
	}-*/;

	public final native JsArrayString getAudioCodecs() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.audioCodecs
	}-*/;

	public final native void setAudioCodecs(JsArrayString audioCodecs) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.audioCodecs = audioCodecs;
	}-*/;

	public final native int getAudioDscp() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.audioDscp;
	}-*/;

	public final native void setAudioDscp(int audioDscp) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.audioDscp = audioDscp;
	}-*/;

	public final native int getAudioJittcomp() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.audioJittcomp;
	}-*/;

	public final native void setAudioJittcomp(int audioJittcomp) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.audioJittcomp = audioJittcomp;
	}-*/;

	public final native int getAudioPort() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.audioPort;
	}-*/;

	public final native void setAudioPort(int audioPort) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.audioPort = audioPort;
	}-*/;

	public final native JsArray<AuthenticationInfo> getAuthInfoList() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.authInfoList;
	}-*/;

	public final native JsArray<CallLog> getCallLogs() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.callLogs;
	}-*/;

	public final native JsArray<Call> getCalls() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.calls;
	}-*/;

	public final native int getCallsCount() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.callsNb;
	}-*/;

	public final native String getCaptureDevice() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.captureDevice;
	}-*/;

	public final native void setCaptureDevice(String device) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.captureDevice = device;
	}-*/;

	public final native String getChatDatabasePath() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.chatDatabasePath;
	}-*/;

	public final native void setChatDatabasePath(String databasePath) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.chatDatabasePath = databasePath;
	}-*/;

	public final native JsArray<ChatRoom> getChatRooms() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.chatRooms;
	}-*/;

	public final native int getConferenceLocalInputVolume() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.conferenceLocalInputVolume;
	}-*/;

	public final native int getConferenceSize() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.conferenceSize;
	}-*/;

	public final native Configuration getConfig() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.config;
	}-*/;

	public final native Call getCurrentCall() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.currentCall;
	}-*/;

	public final native ProxyConfig getDefaultProxy() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.defaultProxy;
	}-*/;

	public final native int getDelayedTimeout() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.delayedTimeout;
	}-*/;

	public final native void setDelayedTimeout(int timeout) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.delayedTimeout = timeout;
	}-*/;

	public final native int getDeviceOrientation() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.deviceOrientation;
	}-*/;

	public final native void setDeviceOrientation(int orientation) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.deviceOrientation = orientation;
	}-*/;

	public final native int getDownloadBandwidth() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.downloadBandwidth;
	}-*/;

	public final native void setDownloadBandwidth(int bandwidth) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.downloadBandwidth = bandwidth;
	}-*/;

	public final native int getDownloadPtime() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.downloadPtime;
	}-*/;

	public final native void setDownloadPtime(int ptime) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.downloadPtime = ptime;
	}-*/;

	public final native boolean getEchoCancellationEnabled() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.echoCancellationEnabled;
	}-*/;

	public final native void setEchoCancellationEnabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.echoCancellationEnabled = enabled;
	}-*/;

	public final native boolean getEchoLimiterEnabled() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.echoLimiterEnabled;
	}-*/;

	public final native void setEchoLimiterEnabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.echoLimiterEnabled = enabled;
	}-*/;

	// TODO implementa con enum
	public final native int getFirewallPolicy() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.firewallPolicy;
	}-*/;

	// TODO implementa con enum
	public final native void setFirewallPolicy(int policy) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.firewallPolicy = policy;
	}-*/;

	public final native JsArray<Friend> getFriendsList() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.friendsList;
	}-*/;

	public final native boolean isGuessHostname() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.guessHostname;
	}-*/;

	public final native void setGuessHostname(boolean guessHostname) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.guessHostname = guessHostname;
	}-*/;

	public final native String getIdentity() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.identity;
	}-*/;

	public final native void setIdentity(String identity) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.identity = identity;
	}-*/;

	public final native int getInCallTimeout() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.inCallTimeout;
	}-*/;

	public final native void setInCallTimeout(int timeout) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.inCallTimeout = timeout;
	}-*/;

	public final native boolean isInConference() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.inConference;
	}-*/;

	public final native int getIncomingTimeout() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.incTimeout;
	}-*/;

	public final native void setIncomingTimeout(int timeout) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.incTimeout = timeout;
	}-*/;

	public final native boolean isIpv6Enabled()/*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.ipv6Enabled;
	}-*/;

	public final native void setIpv6Enabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.ipv6Enabled = enabled;
	}-*/;

	public final native boolean isKeepAliveEnabled() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.keepAliveEnabled;
	}-*/;

	public final native void setKeepAliveEnabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.keepAliveEnabled = enabled;
	}-*/;

	public final native int getMaxCalls() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.maxCalls;
	}-*/;

	public final native void setMaxCalls(int maxCalls) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.maxCalls = maxCalls;
	}-*/;

	public final native int getMediaEncryption() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.mediaEncryption;
	}-*/;

	// TODO implementa con enum
	public final native void setMediaEncryption(int mediaEncryption) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		coremediaEncryption = mediaEncryption;
	}-*/;

	public final native boolean isMediaEncryptionMandatory() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.mediaEncryptionMandatory;
	}-*/;

	public final native void setMediaEncryptionMandatory(boolean mandatory) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.mediaEncryptionMandatory = mandatory;
	}-*/;

	public final native void setMicEnabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.micEnabled = enabled;
	}-*/;

	public final native boolean isMicEnabled() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.micEnabled;
	}-*/;

	public final native int getMicGainDb() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.micGainDb;
	}-*/;

	public final native void setMicGainDb(int micGainDb) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.micGainDb = micGainDb;
	}-*/;

	public final native int getMissedCallsCount() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.missedCallsCount;
	}-*/;

	public final native int getMtu()/*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.mtu;
	}-*/;

	public final native void setMtu(int mtu) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.mtu = mtu;
	}-*/;

	public final native String getNatAddress() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.natAddress;
	}-*/;

	public final native void setNatAddress(String natAddress) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.natAddress = natAddress;
	}-*/;

	public final native int getNativePreviewWindowId() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.nativePreviewWindowId;
	}-*/;

	public final native void setNativePreviewWindowId(int id) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.nativePreviewWindowId = id;
	}-*/;

	public final native int getNativeVideoWindowId() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.nativeVideoWindowId;
	}-*/;

	public final native void setNativeVideoWindowId(int id) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.nativeVideoWindowId = id;
	}-*/;

	public final native boolean isNetworkReachable() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.networkReachable;
	}-*/;

	public final native void setNetworkReachable(boolean reachable) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.networkReachable = reachable;
	}-*/;

	public final native int getNortpTimeout() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.nortpTimeout;
	}-*/;

	public final native void setNortpTimeout(int timeout) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.nortpTimeout = timeout;
	}-*/;

	public final native int getPlayLevel()/*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.playLevel;
	}-*/;

	public final native void setPlayLevel(int level) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.playLevel = level;
	}-*/;

	public final native String getPlaybackDevice() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.playbackDevice;
	}-*/;

	public final native void setPlaybackDevice(String device) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.playbackDevice = device;
	}-*/;

	public final native int getPlaybackGainDb()/*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.playbackGainDb;
	}-*/;

	public final native void setPlaybackGainDb(int gain) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.playbackGainDb = gain;
	}-*/;

	public final native VideoSize getPreferredVideoSize() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.preferredVideoSize;
	}-*/;

	public final native void setPreferredVideoSize(VideoSize size) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.preferredVideoSize = size;
	}-*/;

	public final native String getPreferredVideoSizeByName() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.preferredVideoSizeByName;
	}-*/;

	public final native void setPreferredVideoSizeByName(String name) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		corepreferredVideoSizeByName = name;
	}-*/;

	public final native PresenceModel getPresenceModel() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.presenceModel;
	}-*/;

	public final native void setPresenceModel(PresenceModel model) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.presenceModel = model;
	}-*/;

	public final native String getPrimaryContact() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.primaryContact;
	}-*/;

	public final native void setPrimaryContact(String contact) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.primaryContact = contact;
	}-*/;

	public final native Address getPrimaryContactParsed() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.primaryContactParsed;
	}-*/;

	public final native void setPrimaryContactParsed(Address contact) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.primaryContactParsed = contact;
	}-*/;

	public final native  JsArray<ProxyConfig> getProxyConfigList() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.proxyConfigList;
	}-*/;

	public final native int getRecLevel() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.recLevel;
	}-*/;

	public final native void setRecLevel(int level) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.recLevel = level;
	}-*/;

	public final native String getRing()/*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.ring;
	}-*/;

	public final native void setRing(String ring) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.ring = ring;
	}-*/;

	public final native int getRingLevel() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.ringLevel;
	}-*/;

	public final native void setRingLevel(int level) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.ringLevel = level;
	}-*/;

	public final native String getRingback() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.ringback;
	}-*/;

	public final native void setRingback(String ringback) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.ringback = ringback;
	}-*/;

	public final native String getRingerDevice() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.ringerDevice;
	}-*/;

	public final native void setRingerDevice(String device) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.ringerDevice = device;
	}-*/;

	public final native String getRootCa() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.rootCa;
	}-*/;

	public final native void setRootCa(String rootCa) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.rootCa = rootCa;
	}-*/;

	public final native boolean isSelfViewEnabled() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.selfViewEnabled;
	}-*/;

	public final native void setSelfViewEnabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.selfViewEnabled = enabled;
	}-*/;

	public final native int getSipDscp() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.sipDscp;
	}-*/;

	public final native void setSipDscp(int sipDscp) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.sipDscp = sipDscp;
	}-*/;

	public final native int getSipPort() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.sipPort;
	}-*/;

	public final native void setSipPort(int port) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.sipPort = port;
	}-*/;

	public final native SipTransports getSipTransports() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.sipTransports;
	}-*/;

	public final native void setSipTransports(SipTransports transports) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.sipTransports = transports;
	}-*/;

	public final native JsArrayString getSoundDevices() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.soundDevices;
	}-*/;

	public final native String getStaticPicture() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.staticPicture;
	}-*/;

	public final native void setStaticPicture(String path)  /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.staticPicture = path;
	}-*/;

	public final native int getStaticPictureFps() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.staticPictureFps;
	}-*/;

	public final native void setStaticPictureFps(int fps) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.staticPictureFps = fps;
	}-*/;

	public final native String getStunServer() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.stunServer;
	}-*/;

	public final native void setStunServer(String server) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.stunServer = server;
	}-*/;

	public final native JsArray<VideoSizeDef> getSupportedVideoSizes() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.supportedVideoSizes;
	}-*/;

	public final native int getUploadBandwidth() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.uploadBandwidth;
	}-*/;

	public final native void setUploadBandwidth(int bandwidth) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.uploadBandwidth = bandwidth;
	}-*/;

	public final native int getUploadPtime() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.uploadPtime;
	}-*/;

	public final native void setUploadPtime(int ptime) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.uploadPtime = ptime;
	}-*/;

	public final native String getUpnpExternalIpaddress() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.upnpExternalIpaddress;
	}-*/;

	public final native void setUpnpExternalIpaddress(String address) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.upnpExternalIpaddress = address;
	}-*/;

	// TODO implementa con enum
	public final native int getUpnpState() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.upnpState;
	}-*/;

	public final native boolean useInfoForDtmf() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.useInfoForDtmf;
	}-*/;

	public final native void setUseInfoForDtmf(boolean use) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.useInfoForDtmf = use;
	}-*/;

	public final native boolean useRfc2833ForDtmf() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.useRfc2833ForDtmf;
	}-*/;

	public final native void setUseRfc2833ForDtmf(boolean use) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.useRfc2833ForDtmf = use;
	}-*/;

	public final native boolean isVideoAdaptiveJittcompEnabled() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.videoAdaptiveJittcompEnabled;
	}-*/;

	public final native void setVideoAdaptiveJittcompEnabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.videoAdaptiveJittcompEnabled = enabled;
	}-*/;

	public final native boolean isVideoCaptureEnabled() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.videoCaptureEnabled;
	}-*/;

	public final native void setVideoCaptureEnabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.videoCaptureEnabled = enabled;
	}-*/;

	public final native JsArrayString getVideoCodecs() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.videoCodecs;
	}-*/;

	public final native void setVideoCodecs(JsArrayString codecs) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.videoCodecs = codecs;
	}-*/;

	public final native String getVideoDevice() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.videoDevice;
	}-*/;

	public final native void setVideoDevice(String device) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.videoDevice = device;
	}-*/;

	public final native JsArrayString getVideoDevices() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.videoDevices;
	}-*/;

	public final native boolean isVideoDisplayEnabled() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.videoDisplayEnabled;
	}-*/;

	public final native void setVideoDisplayEnabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.videoDisplayEnabled = enabled;
	}-*/;

	public final native String getVideoDisplayFilter() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.videoDisplayFilter;
	}-*/;

	public final native void setVideoDisplayFilter(String filter) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.videoDisplayFilter = filter;
	}-*/;

	public final native int getVideoDscp() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.videoDscp;
	}-*/;

	public final native void setVideoDscp(int dscp) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.videoDscp = dscp;
	}-*/;

	public final native int getVideoJittcomp()/*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.videoJittcomp;
	}-*/;

	public final native void setVideoJittcomp(int jittcomp) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.videoJittcomp = jittcomp;
	}-*/;

	// TODO Gestione tramite enum
	public final native int getVideoPolicy() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.videoPolicy;
	}-*/;

	// TODO Gestione tramite enum
	public final native void setVideoPolicy(int videoPolicy) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.videoPolicy = videoPolicy;
	}-*/;

	public final native int getVideoPort() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.videoPort;
	}-*/;

	public final native void setVideoPort(int port) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.videoPort = port;
	}-*/;

	public final native boolean isVideoPreviewEnabled() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.videoPreviewEnabled;
	}-*/;

	public final native void setVideoPreviewEnabled(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.videoPreviewEnabled = enabled;
	}-*/;

	public final native String getZrtpSecretsFile() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.zrtpSecretsFile;
	}-*/;

	public final native void setZrtpSecretsFile(String file) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.zrtpSecretsFile = file;
	}-*/;

	public final native void abortAuthentication(AuthenticationInfo info) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.abortAuthentication(info);
	}-*/;

	public final native int acceptCall(Call call) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.acceptCall(call);
	}-*/;

	public final native int acceptCallUpdate(Call call, CallParameters params) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.acceptCallUpdate(call, params);
	}-*/;

	public final native int acceptCallWithParams(Call call, CallParameters params) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.acceptCallWithParams(call, params);
	}-*/;

	public final native int addAllToConference() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.addAllToConference();
	}-*/;

	public final native void addFriend(Friend friend) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.addFriend(friend);
	}-*/;

	public final native void addToConference(Call call) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.addToConference(call);
	}-*/;

	public final native boolean canWeAddCall() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.canWeAddCall();
	}-*/;

	public final native void clearAllAuthInfo() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.clearAllAuthInfo();
	}-*/;

	public final native void clearCallLogs() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.clearCallLogs();
	}-*/;

	public final native void clearProxyConfig() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.clearProxyConfig();
	}-*/;

	// TODO implementa con enum
	public final native int declineCall(Call call, int reason) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.declineCall(call, reason);
	}-*/;

	public final native int deferCalUpdate(Call call) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.deferCalUpdate(call);
	}-*/;

	public final native int enablePayloadType(int type, boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.enablePayloadType(type, enabled);
	}-*/;

	public final native int enterConference() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.enterConference();
	}-*/;

	public final native AuthenticationInfo findAuthInfo(String realm, String username) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.findAuthInfo(realm, username);
	}-*/;

	public final native Call findCallFromUri(String uri) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.findCallFromUri(uri);
	}-*/;

	public final native int findPayloadType(String type, int rate, int channels) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.findPayloadType(type, rate, channels);
	}-*/;

	public final native void getAudioPortRange(int min, int max) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.getAudioPortRange(min, max);
	}-*/;

	public final native Call getCallByRemoteAddress(String remote) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.getCallByRemoteAddress(remote);
	}-*/;

	public final native ChatRoom getChatRoom(Address address) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.getChatRoom(address);
	}-*/;

	public final native int getDefaultProxy(ProxyConfig config) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.getDefaultProxy();
	}-*/;

	public final native Friend getFriendByAddress(Address address) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.getFriendByAddress(address);
	}-*/;

	public final native Friend getFriendByRefKey(String key) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.getFriendByRefKey(key);
	}-*/;

	public final native ChatRoom getOrCreateChatRooom(String destination) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.getOrCreateChatRooom(destination);
	}-*/;

	public final native int getSipTransportsPorts() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.getSipTransports();
	}-*/;

	public final native void getVideoPortRange(int min, int max) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.getVideoPortRange(min, max);
	}-*/;

	public final native boolean isInCall() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.inCall();
	}-*/;

	public final native Address interpretUrl(String url) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.interpretUrl(url);
	}-*/;

	public final native Call invite(String address)/*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.invite(address);
	}-*/;

	public final native Call inviteAddressWithParams(Address address, CallParameters params) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.inviteAddressWithParams(address, params);
	}-*/;

	public final native Call inviteWithParams(String url, CallParameters params) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.inviteWithParams(url, params);
	}-*/;

	public final native void iterate() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.iterate();
	}-*/;

	public final native int leaveConference() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.leaveConference();
	}-*/;

	public final native ChatRoom newChatRoom(String address) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newChatRoom(address);
	}-*/;

	public final native CallParameters newDefaultCallParameters() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newDefaultCallParameters();
	}-*/;

	public final native Friend newFriend() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newFriend();
	}-*/;

	public final native Friend newFriendWithAddress(String address) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newFriendWithAddress(address);
	}-*/;

	public final native Configuration newConfig(String filename) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newConfig(filename);
	}-*/;

	// TODO implementa con enum
	public final native PresenceActivity newPresenceActivity(int activityType, String description) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newPresenceActivity(activityType, description);
	}-*/;

	public final native PresenceModel newPresenceModel() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newPresenceModel();
	}-*/;

	// TODO implementa con enum
	public final native PresenceModel newPresenceModelWithActivity(int activityType, String description) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newPresenceModelWithActivity(activityType, description);
	}-*/;

	public final native PresenceModel newPresenceModelWithActivityAndNote(int activityType, String description, String note, String lang) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newPresenceModelWithActivityAndNote(activityType, description, note, lang);
	}-*/;

	public final native PresenceNote newPresenceNote(String content, String lang)/*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newPresenceNote(content, lang);
	}-*/;

	public final native PresencePerson newPresencePerson(String id) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newPresencePerson(id);
	}-*/;

	// TODO implementa con enum
	public final native PresenceService newPresenceService(String id, int presenceBasicStatus, String contact) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.newPresenceService(id, presenceBasicStatus, contact);
	}-*/;

	public final native void notifyAllFriends(PresenceModel model) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.notifyAllFriends(model);
	}-*/;

	public final native int pauseAllCalls() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.pauseAllCalls();
	}-*/;

	public final native int pauseCall(Call call) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.pauseCall(call);
	}-*/;

	// TODO implementa con enum
	public final native boolean payloadTypeEnabled(int payloadType) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.payloadTypeEnabled(payloadType);
	}-*/;

	// TODO implementa con enum
	public final native void playDtmf(String dtmf,int millis) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.playDtmf(dtmf, millis);
	}-*/;

	public final native LinphoneEvent publish(Address address, String event, int lifetime, Content body) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.publish(address, event, lifetime, body);
	}-*/;

	public final native int redirectCall(Call call, String uri) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.redirectCall(call, uri);
	}-*/;

	public final native void refreshRegisters() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.refreshRegisters();
	}-*/;

	public final native void rejectSubscriber(Friend friend) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.rejectSubscriber(friend);
	}-*/;

	public final native void reloadSoundDevices() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.reloadSoundDevices();
	}-*/;

	public final native void reloadVideoDevices() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.reloadVideoDevices();
	}-*/;

	public final native void removeAuthInfo(AuthenticationInfo info) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.removeAuthInfo(info);
	}-*/;

	public final native void removeCallLog(CallLog log) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.removeCallLog(log);
	}-*/;

	public final native void removeFriend(Friend friend) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.removeFriend(friend);
	}-*/;

	public final native int removeFromConference(Call call) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.removeFromConference(call);
	}-*/;

	public final native void removeProxyConfig(ProxyConfig config) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.removeProxyConfig(config);
	}-*/;

	public final native void resetMissedCallsCount() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.resetMissedCallsCount();
	}-*/;

	public final native int resumeCall(Call call) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.resumeCall(call);
	}-*/;

	// TODO implementa con enum
	public final native void sendDtmf(String dtmf) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.sendDtmf(dtmf);
	}-*/;

	public final native void setAudioPortRange(int min, int max) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.setAudioPortRange(min, max);
	}-*/;

	public final native void setUserAgent(String name, String version) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.setUserAgent(name, version);
	}-*/;

	public final native void setVideoPortRange(int min, int max) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.setVideoPortRange(min, max);
	}-*/;

	public final native boolean soundDeviceCanCapture(String device) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.soundDeviceCanCapture(device);
	}-*/;

	public final native boolean soundDeviceCanPlayback(String device) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.soundDeviceCanPlayback(device);
	}-*/;

	public final native boolean soundResourcesLocked() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.soundResourcesLocked();
	}-*/;

	public final native void startDtmfStream() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.startDtmfStream();
	}-*/;

	public final native void stopDtmf() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.stopDtmf();
	}-*/;

	public final native void stopDtmfStream() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.stopDtmfStream();
	}-*/;

	public final native void stopRinging()/*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.stopRinging();
	}-*/;

	public final native LinphoneEvent subscribe(Address address, String event, int duration, Content content) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.subscribe(address, event, duration, content);
	}-*/;

	public final native int terminateAllCalls() /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.terminateAllCalls();
	}-*/;

	public final native int terminateCall(Call call) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.terminateCall(call);
	}-*/;

	public final native int terminateConference()/*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.terminateConference();
	}-*/;

	public final native int transferCall(Call call, String to) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.transferCall(call, to);
	}-*/;

	public final native int transferCallToAnother(Call from, Call to)/*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.transferCallToAnother(from, to);
	}-*/;

	public final native int updateCall(Call call, CallParameters params) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		return core.updateCall(call, params);
	}-*/;

	public final native void usePreviewWindow(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.usePreviewWindow(enabled);
	}-*/;

	public final native void verifyServerCertificates(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.verifyServerCertificates(enabled);
	}-*/;

	public final native void verifyServerCn(boolean enabled) /*-{
		var core = this.@it.netgrid.gwt.linphone.Linphone::coreElement;
		core.verifyServerCn(enabled);
	}-*/;

	public final native int uninit()/*-{
		return this.uninit();
	}-*/;

}
