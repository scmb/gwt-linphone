package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class LinphoneEvent extends JavaScriptObject {
	protected LinphoneEvent() {}

	public final native Address getFrom()/*-{
		return this.from;
	}-*/;

	public final native String getName()/*-{
		return this.name;
	}-*/;

	// TODO implementa con enum
	public final native int getPublishState()/*-{
		return this.publishState;
	}-*/;

	// TODO implementa con enum
	public final native int getReason() /*-{
		return this.reason;
	}-*/;

	public final native Address getResource()/*-{
		return this.resource;
	}-*/;

	// TODO implementa con enum
	public final native int getSubscriptionDir() /*-{
		return this.subscriptionDir;
	}-*/;

	// TODO implementa con enum
	public final native int getSubscriptionState() /*-{
		return this.subscriptionState;
	}-*/;

	public final native int acceptSubscription() /*-{
		return this.acceptSubscription();
	}-*/;

	// TODO implementa con enum
	public final native int denySubscription(int reason) /*-{
		return this.denySubscription(reason);
	}-*/;

	public final native int notify(Content body) /*-{
		return this.notify(body);
	}-*/;

	public final native void terminate() /*-{
		this.terminate();
	}-*/;

	public final native int updatePublish(Content body) /*-{
		return this.updatePublish(body);
	}-*/;

	public final native int updateSubscribe(Content body) /*-{
		return this.updateSubscribe(body);
	}-*/;
}
