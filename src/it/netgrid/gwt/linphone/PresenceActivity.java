package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class PresenceActivity extends JavaScriptObject {
	protected PresenceActivity() {}

	public final native String getDescription() /*-{
		return this.description;
	}-*/;

	public final native void setDescription(String description)/*-{
		this.description = description;
	}-*/;

	// TODO implementa con enum
	public final native int getType()/*-{
		return this.type;
	}-*/;

	public final native String getString()/*-{
		return this.toString();
	}-*/;

}
