package it.netgrid.gwt.linphone;

import it.netgrid.gwt.linphone.enums.PresenceActivityType;

import java.util.Date;

import com.google.gwt.core.client.JavaScriptObject;

public class PresenceModel extends JavaScriptObject {
	protected PresenceModel() {}

	public final native void setActivity(PresenceActivityType presence) /*-{
		this.setActivity(presence, null);
	}-*/;

	public final native String getActivity() /*-{
		return this.activity;
	}-*/;

	// TODO implementa con enum
	public final native int getBasicStatus() /*-{
		return this.basicStatus;
	}-*/;

	// TODO implementa con enum
	public final native void setBasicStatus(int status) /*-{
		this.basicStatus = status;
	}-*/;

	public final native String getContact()/*-{
		return this.contact;
	}-*/;

	public final native void setContact(String contact) /*-{
		this.contact = contact;
	}-*/;

	public final native int getActivitiesCount()/*-{
		return this.nbActivities;
	}-*/;

	public final native int getPersonsCount()/*-{
		return this.nbPersons;
	}-*/;

	public final native int getServicesCount()/*-{
		return this.nbServices;
	}-*/;

	public final native Date getTimestamp()/*-{
		return this.timestamp;
	}-*/;

	public final native int addActivity(PresenceActivity activity) /*-{
		return this.addActivity(activity);
	}-*/;

	public final native int addNote(String content, String lang) /*-{
		return this.addNote(content, lang);
	}-*/;

	public final native int addPerson(PresencePerson person) /*-{
		return this.addPerson(person);
	}-*/;

	public final native int addService(PresenceService service) /*-{
		return this.addService(service);
	}-*/;

	public final native int clearActivities() /*-{
		return this.clearActivities();
	}-*/;

	public final native int clearNotes() /*-{
		return this.clearNotes();
	}-*/;

	public final native int clearPersons()/*-{
		return this.clearPersons();
	}-*/;

	public final native int clearServices()/*-{
		return this.clearServices();
	}-*/;

	public final native PresenceNote getNote(String lang) /*-{
		return this.getNote(lang);
	}-*/;

	public final native PresenceActivity getNthActivity(int id)/*-{
		this.getNthActivity(id);
	}-*/;

	public final native PresencePerson getNthPerson(int id) /*-{
		return this.getNthPerson(id);
	}-*/;

	public final native PresenceService getNthService(int id)/*-{
		return this.getNthService(id);
	}-*/;

	// TODO implementa con enum
	public final native int setActivity(int type, String description) /*-{
		return this.setActivity(type, description);
	}-*/;

}
