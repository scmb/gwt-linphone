package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class PresenceNote extends JavaScriptObject {
	protected PresenceNote() {}


	public final native String getContent()/*-{
		return this.content;
	}-*/;

	public final native void setContent(String content) /*-{
		this.content = content;
	}-*/;

	public final native String getLang()/*-{
		return this.lang;
	}-*/;

	public final native void setLang(String lang)/*-{
		this.lang = lang;
	}-*/;

}
