package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class PresencePerson extends JavaScriptObject {
	protected PresencePerson() {}

	public final native String getId()/*-{
		return this.id;
	}-*/;

	public final native void setId(String id) /*-{
		this.id = id;
	}-*/;

	public final native int getActivitiesCount() /*-{
		return this.nbActivities;
	}-*/;

	public final native int getActivitiesNotesCount() /*-{
		return this.nbActivitiesNotes;
	}-*/;

	public final native int getNotesCount() /*-{
		return this.nbNotes;
	}-*/;

	public final native int addActivitiesNote(PresenceNote note) /*-{
		return this.addActivitiesNote(note);
	}-*/;

	public final native int addActivity(PresenceActivity activity)/*-{
		return this.addActivity(activity);
	}-*/;

	public final native int addNote(PresenceNote note)/*-{
		return this.addNote(note);
	}-*/;

	public final native int clearActivities() /*-{
		return this.clearActivities();
	}-*/;

	public final native int clearActivitiesNotes() /*-{
		return this.clearActivitiesNotes();
	}-*/;

	public final native int clearNotes()/*-{
		return this.clearNotes();
	}-*/;

	public final native PresenceActivity getNthActivity(int index)/*-{
		return this.getNthActivity(index);
	}-*/;

	public final native PresenceNote getNthActivitiesNote(int index) /*-{
		return this.getNthActivitiesNote(index);
	}-*/;

	public final native PresenceNote getNthNote(int index)/*-{
		return this.getNthNote(index);
	}-*/;

}
