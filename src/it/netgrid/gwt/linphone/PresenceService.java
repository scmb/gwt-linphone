package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class PresenceService extends JavaScriptObject {
	protected PresenceService() {}

	//TODO implementa con enum
	public final native int getBasicStatus()/*-{
		return this.basicStatus;
	}-*/;

	// TODO implementa con enum
	public final native void setBasicStatus(int basicStatus)/*-{
		this.basicStatus = basicStatus;
	}-*/;

	public final native String getContact()/*-{
		return this.contact;
	}-*/;

	public final native void setContact(String contact) /*-{
		this.contact = contact;
	}-*/;

	public final native String getId()/*-{
		return this.id;
	}-*/;

	public final native void setId(String id) /*-{
		this.id = id;
	}-*/;

	public final native int getNotesCount()/*-{
		return this.nbNotes;
	}-*/;

	public final native void addNote(PresenceNote note)/*-{
		this.addNote(note);
	}-*/;

	public final native int clearNotes()/*-{
		return this.clearNotes();
	}-*/;

	public final native PresenceNote getNthNote(int index)/*-{
		return this.getNthNote(index);
	}-*/;

}
