package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class ProxyConfig extends JavaScriptObject {

	protected ProxyConfig() {
	}

	public final native String getIdentity() /*-{
		return this.identity;
	}-*/;

	public final native String getServerAddr() /*-{
		return this.serverAddr;
	}-*/;

	public final native boolean isRegisterEnabled() /*-{
		return this.registerEnabled;
	}-*/;

	public final native void setIdentity(String value) /*-{
		this.identity = value;
	}-*/;

	public final native void setRegisterEnabled(boolean value) /*-{
		this.registerEnabled = value;
	}-*/;

	public final native void setServerAddr(String value) /*-{
		this.serverAddr = value;
	}-*/;

	public final native String getContactParameters()/*-{
		return this.contactParameters;
	}-*/;

	public final native void setContactParameters(String parameters)/*-{
		this.contactParameters = parameters;
	}-*/;

	public final native Linphone getCore()/*-{
		return this.core;
	}-*/;

	public final native boolean isDialEscapePlus()/*-{
		return this.dialEscapePlus;
	}-*/;

	public final native void setDialEscapePlus(boolean escape)/*-{
		this.dialEscapePlus = escape;
	}-*/;

	public final native String getDialPrefix()/*-{
		return this.dialPrefix;
	}-*/;

	public final native void setDialPrefix(String dialPrefix)/*-{
		this.dialPrefix = dialPrefix;
	}-*/;

	public final native String getDomain()/*-{
		return this.domain;
	}-*/;

	// TODO implementa con enum
	public final native int getError()/*-{
		return this.error;
	}-*/;

	public final native int getExpires()/*-{
		return this.expires;
	}-*/;

	public final native void setExpires(int seconds)/*-{
		this.expires = seconds;
	}-*/;

	public final native PrivacyMask getPrivacy()/*-{
		return this.privacy;
	}-*/;

	public final native void setPrivacy(PrivacyMask mask)/*-{
		this.privacy = mask;
	}-*/;

	public final native boolean isPublishEnabled()/*-{
		return this.publishEnabled;
	}-*/;

	public final native void setPublishEnabled(boolean enabled) /*-{
		this.publishEnabled = enabled;
	}-*/;

	public final native String getRoute()/*-{
		return this.route;
	}-*/;

	public final native void setRoute(String route)/*-{
		this.route = route;
	}-*/;

	// TODO implementa con enum
	public final native int getState()/*-{
		return this.state;
	}-*/;

	public final native void done()/*-{
		this.done();
	}-*/;

	public final native void edit()/*-{
		this.edit();
	}-*/;

	public final native int normalizeNumber(String username, String result, int length)/*-{
		return this.normalizeNumber(username, result, length);
	}-*/;

	public final native void refreshRegister()/*-{
		this.refreshRegister();
	}-*/;

}
