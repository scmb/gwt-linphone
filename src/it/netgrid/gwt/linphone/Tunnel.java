package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class Tunnel extends JavaScriptObject {
	protected Tunnel() {}

	public final native void addServer(TunnelConfig config)/*-{
		this.addServer(config);
	}-*/;

	public final native void autoDetect()/*-{
		this.autoDetect();
	}-*/;

	public final native void enable(boolean enable)/*-{
		this.enable(enable);
	}-*/;

	public final native void getHttpProxy(String host, int port, String username, String password)/*-{
		this.getHttpProxy(host, port, username, password);
	}-*/;

	public final native void removeServer(TunnelConfig config)/*-{
		this.removeServer(config);
	}-*/;

	public final native void setHttpProxy(String host, int port, String username, String password)/*-{
		this.setHttpProxy(host, port, username, password);
	}-*/;

}
