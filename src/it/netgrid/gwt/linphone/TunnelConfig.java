package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public class TunnelConfig extends JavaScriptObject {
	protected TunnelConfig() {}

	public final native int getDelay()/*-{
		return this.delay;
	}-*/;

	public final native void setDelay(int delay)/*-{
		this.delay = delay;
	}-*/;

	public final native String getHost()/*-{
		return this.host;
	}-*/;

	public final native void setHost(String host)/*-{
		this.host = host;
	}-*/;

	public final native int getPort()/*-{
		return this.port;
	}-*/;

	public final native void setPort(int port)/*-{
		this.port = port;
	}-*/;

	public final native int getRemoteUdpMirrorPort()/*-{
		return this.remoteUdpMirrorPort;
	}-*/;

	public final native void setRemoteUdpMirrorPort(int port)/*-{
		this.remoteUdpMirrorPort = port;
	}-*/;

}
