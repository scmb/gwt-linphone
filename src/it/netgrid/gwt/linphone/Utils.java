package it.netgrid.gwt.linphone;

import com.google.gwt.core.client.JavaScriptObject;

public final class Utils {

	public static native JavaScriptObject getConfig() /*-{
		return $wnd.LinphoneUtils.getConfig();
	}-*/;

}
