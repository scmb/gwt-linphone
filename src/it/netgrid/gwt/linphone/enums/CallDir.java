package it.netgrid.gwt.linphone.enums;

public enum CallDir {

	Unknown(-1),
	Outgoing(0),
	Incoming(1);

	private int code;
	private CallDir(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static CallDir getByCode(int code) {
		for(CallDir error : CallDir.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return CallDir.Unknown;
	}
}
