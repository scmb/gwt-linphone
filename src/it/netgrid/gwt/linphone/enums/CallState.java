package it.netgrid.gwt.linphone.enums;

public enum CallState {

	Unknown(-1),
	Idle(0),
	IncomingReceived(1),
	OutgoingInit(2),
	OutgoingProgress(3),
	OutgoingRinging(4),
	OutgoingEarlyMedia(5),
	Connected(6),
	StreamsRunning(7),
	Pausing(8),
	Paused(9),
	Resuming(10),
	Refered(11),
	Error(12),
	End(13),
	PausedByRemote(14),
	UpdatedByRemote(15),
	IncomingEarlyMedia(16),
	Updated(17),
	Released(18);


	private int code;
	private CallState(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static CallState getByCode(int code) {
		for(CallState error : CallState.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return CallState.Unknown;
	}
}
