package it.netgrid.gwt.linphone.enums;

public enum CallStatus {

	Unknown(-1),
	Success(0),
	Aborted(1),
	Missed(2),
	Declined(3);

	private int code;
	private CallStatus(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static CallStatus getByCode(int code) {
		for(CallStatus error : CallStatus.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return CallStatus.Unknown;
	}
}
