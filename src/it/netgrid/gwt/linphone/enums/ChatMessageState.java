package it.netgrid.gwt.linphone.enums;

public enum ChatMessageState {

	Unknown(-1),
	Idle(0),
	InProgress(1),
	Delivered(2),
	NotDelivered(3);


	private int code;
	private ChatMessageState(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static ChatMessageState getByCode(int code) {
		for(ChatMessageState error : ChatMessageState.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return ChatMessageState.Unknown;
	}
}
