package it.netgrid.gwt.linphone.enums;

public enum ErrorCode {
	Unknown(-1),
	None(0),
	NoResponse(1),
	BadCredentials(2),
	Declined(3),
	NotFound(4),
	NotAnswered(5),
	Busy(6),
	Media(7),
	IOError(8),
	DoNotDisturb(9),
	Unauthorized(10);

	private int code;
	private ErrorCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static ErrorCode getByCode(int code) {
		for(ErrorCode error : ErrorCode.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return ErrorCode.Unknown;
	}
}
