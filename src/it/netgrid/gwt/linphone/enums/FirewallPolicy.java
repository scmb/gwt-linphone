package it.netgrid.gwt.linphone.enums;

public enum FirewallPolicy {

	Unknown(-1),
	NoFirewall(0),
	UseNatAddress(1),
	UseStun(2),
	UseIce(3),
	UseUpnp(4);

	private int code;
	private FirewallPolicy(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static FirewallPolicy getByCode(int code) {
		for(FirewallPolicy error : FirewallPolicy.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return FirewallPolicy.Unknown;
	}
}
