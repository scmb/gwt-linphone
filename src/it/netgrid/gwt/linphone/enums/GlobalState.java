package it.netgrid.gwt.linphone.enums;

public enum GlobalState {

	Unknown(-1),
	Off(0),
	Startup(1),
	On(2),
	Shutdown(3);

	private int code;
	private GlobalState(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static GlobalState getByCode(int code) {
		for(GlobalState error : GlobalState.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return GlobalState.Unknown;
	}
}
