package it.netgrid.gwt.linphone.enums;

public enum IceState {

	Unknown(-1),
	NotActivated(0),
	Failed(1),
	InProgress(2),
	HostConnection(3),
	ReflexiveConnection(4),
	RelayConnection(5);

	private int code;
	private IceState(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static IceState getByCode(int code) {
		for(IceState error : IceState.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return IceState.Unknown;
	}
}
