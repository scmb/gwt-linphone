package it.netgrid.gwt.linphone.enums;

public enum MediaEncryption {

	Unknown(-1),
	None(0),
	SRTP(1),
	ZRTP(2);

	private int code;
	private MediaEncryption(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static MediaEncryption getByCode(int code) {
		for(MediaEncryption error : MediaEncryption.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return MediaEncryption.Unknown;
	}
}
