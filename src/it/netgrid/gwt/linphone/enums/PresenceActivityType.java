package it.netgrid.gwt.linphone.enums;

public enum PresenceActivityType {
	Offline(0),
	Online(1),
	Appointment(2),
	Away(3),
	Breakfast(4),
	Busy(5),
	Dinner(6),
	Holiday(7),
	InTransit(8),
	LookingForWork(9),
	Lunch(10),
	Meal(11),
	Meeting(12),
	OnThePhone(13),
	Other(14),
	Performance(15),
	PermanentAbsence(16),
	Playing(17),
	Presentation(18),
	Shopping(19),
	Sleeping(20),
	Spectator(21),
	Steering(22),
	Travel(23),
	TV(24),
	Unknown(25),
	Vacation(26),
	Working(27),
	Worship(28);


	private int code;
	private PresenceActivityType(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static PresenceActivityType getByCode(int code) {
		for(PresenceActivityType error : PresenceActivityType.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return PresenceActivityType.Unknown;
	}
}
