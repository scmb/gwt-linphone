package it.netgrid.gwt.linphone.enums;

public enum PresenceBasicStatus {

	Unknown(-1),
	Open(0),
	Closed(1);

	private int code;
	private PresenceBasicStatus(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static PresenceBasicStatus getByCode(int code) {
		for(PresenceBasicStatus error : PresenceBasicStatus.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return PresenceBasicStatus.Unknown;
	}
}
