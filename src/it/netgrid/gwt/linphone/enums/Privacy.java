package it.netgrid.gwt.linphone.enums;

public enum Privacy {

	Unknown(-1),
	None(0),
	User(1),
	Header(2),
	Session(3),
	Id(4),
	Critical(5),
	Default(6);

	private int code;
	private Privacy(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static Privacy getByCode(int code) {
		for(Privacy error : Privacy.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return Privacy.Unknown;
	}
}
