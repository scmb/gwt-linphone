package it.netgrid.gwt.linphone.enums;

public enum PublishState {

	Unknown(-1),
	None(0),
	Progress(1),
	Ok(2),
	Error(3),
	Expiring(4),
	Cleared(5);

	private int code;
	private PublishState(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static PublishState getByCode(int code) {
		for(PublishState error : PublishState.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return PublishState.Unknown;
	}
}
