package it.netgrid.gwt.linphone.enums;

public enum Reason {

	Unknown(-1),
	None(0),
	NoResponse(1),
	BadCredentials(2),
	Declined(3),
	NotFound(4),
	NotAnswered(5),
	Busy(6),
	Media(7),
	IOError(8),
	DoNotDisturb(9),
	Unauthorized(10),
	NotAcceptable(11);

	private int code;
	private Reason(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static Reason getByCode(int code) {
		for(Reason error : Reason.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return Reason.Unknown;
	}
}
