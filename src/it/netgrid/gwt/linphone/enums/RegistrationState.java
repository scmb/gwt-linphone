package it.netgrid.gwt.linphone.enums;

public enum RegistrationState {

	Unknown(-1),
	None(0),
	Progress(1),
	Ok(2),
	Cleared(3),
	Failed(4);

	private int code;
	private RegistrationState(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static RegistrationState getByCode(int code) {
		for(RegistrationState error : RegistrationState.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return RegistrationState.Unknown;
	}
}
