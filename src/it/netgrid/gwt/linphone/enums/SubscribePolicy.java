package it.netgrid.gwt.linphone.enums;

public enum SubscribePolicy {

	Unknown(-1),
	Wait(0),
	Deny(1),
	Accept(2);

	private int code;
	private SubscribePolicy(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static SubscribePolicy getByCode(int code) {
		for(SubscribePolicy error : SubscribePolicy.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return SubscribePolicy.Unknown;
	}
}
