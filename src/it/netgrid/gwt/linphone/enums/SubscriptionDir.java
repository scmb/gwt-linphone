package it.netgrid.gwt.linphone.enums;

public enum SubscriptionDir {

	Unknown(-1),
	Incoming(0),
	Outgoing(1),
	InvalidDir(2);

	private int code;
	private SubscriptionDir(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static SubscriptionDir getByCode(int code) {
		for(SubscriptionDir error : SubscriptionDir.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return SubscriptionDir.Unknown;
	}
}
