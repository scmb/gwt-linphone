package it.netgrid.gwt.linphone.enums;

public enum SubscriptionState {

	Unknown(-1),
	None(0),
	OutgoingInit(1),
	IncomingReceived(2),
	Pending(3),
	Active(4),
	Terminated(5),
	Error(6);

	private int code;
	private SubscriptionState(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static SubscriptionState getByCode(int code) {
		for(SubscriptionState error : SubscriptionState.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return SubscriptionState.Unknown;
	}
}
