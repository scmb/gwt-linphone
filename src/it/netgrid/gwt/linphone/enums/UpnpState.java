package it.netgrid.gwt.linphone.enums;

public enum UpnpState {

	Unknown(-1),
	Idle(0),
	Pending(1),
	Adding(2),
	Removing(3),
	NotAvailable(4),
	Ok(5),
	Ko(6),
	Blacklisted(7);

	private int code;
	private UpnpState(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static UpnpState getByCode(int code) {
		for(UpnpState error : UpnpState.values()) {
			if(error.code == code) {
				return error;
			}
		}

		return UpnpState.Unknown;
	}
}
