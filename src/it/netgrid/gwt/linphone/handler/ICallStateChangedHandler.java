package it.netgrid.gwt.linphone.handler;

import com.google.gwt.core.client.JavaScriptObject;

public interface ICallStateChangedHandler {
	public void onCallStateChanged(JavaScriptObject call, int state, String message);
}
