package it.netgrid.gwt.linphone.handler;

import it.netgrid.gwt.linphone.ChatMessage;

import com.google.gwt.core.client.JavaScriptObject;

public interface IChatMessageStateChangedHandler {
	public void onMessageStateChanged(ChatMessage message, int chatMessageState, JavaScriptObject userData);
}
