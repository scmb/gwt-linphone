package it.netgrid.gwt.linphone.handler;


public interface IFileActionCompletedHandler {
	public void onSuccess();

	public void onError(String message);
}
