package it.netgrid.gwt.linphone.handler;

import it.netgrid.gwt.linphone.Linphone;

public interface ILoadCompletedHandler {
	public void onLoadCompleted(Linphone linphone);
}
