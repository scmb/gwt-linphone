package it.netgrid.gwt.linphone.handler;

public interface ILogHandler {

	public void onMessage(String level, String message);
}
