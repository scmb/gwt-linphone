package it.netgrid.gwt.linphone.handler;

import it.netgrid.gwt.linphone.enums.RegistrationState;

import com.google.gwt.core.client.JavaScriptObject;

public interface IRegistrationStateChangedHandler {
	public void onRegistrationStateChanged(JavaScriptObject proxy, RegistrationState state, String message);
}
